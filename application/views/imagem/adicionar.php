<div class="container">
    
    <div class="row clear pad-top-10">
        
        <h1><?= $melhoria->titulo; ?></h1>
        
        <p><?= $melhoria->descricao; ?></p>
        
    </div>
    
    <div class="row clear pad-top-10">
        
        <h2>Imagens</h1>
        
        <p>Adicione aqui imagens relacionadas à essa melhoria.</p>
        
    </div>
    
    <div class="row clear pad-top-10">
        
        <?php echo form_open_multipart(base_url("imagem/adicionar/" . $melhoria->id));?>
            
            <div id="images">
                <div class="row clear pad-top-10">
                    <label for="imagem" class="col-3 text-right">Imagem</label>
                    <input type="file" id="imagem" name="imagem" class="col-9"/>
                </div>
                <div class="row clear pad-top-5">
                    <label for="descricao" class="col-3 text-right">Descrição</label>
                    <input type="text" id="descricao" name="descricao" class="col-9"/>
                </div>
            </div>
            
            <div class"row clear pad-top-10">
                <button class="btn right">Enviar Imagem</button>
            </div>
            
        </form>
        
    </div>
    
    <div class="row clear pad-top-10">
    
        <h2>Imagens Adicionadas</h2>
        
        <table>
            
            <tr>
                <th>Imagem</th>
                <th>Descrição</th>
                <th>Ações</th>
            </tr>
            
            <?php if ($imagens) { ?>
                <?php foreach($imagens as $imagem) { ?>
                    <tr>
                        <td><img src="<?= $imagem->url; ?>" height="80px"/></td>
                        <td><?= $imagem->descricao; ?></td>
                        <td>
                            <a href="<?= $imagem->url; ?>">Visualizar</a>
                            <a href="<?= base_url("imagem/remover/" . $imagem->id); ?>">Remover</a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td>Sem Imagens</td>
                    <td>Cadastre imagens clicando no botão de adicionar imagens</td>
                </tr>
            <?php } ?>
            
        </table>
        
    </div>
    
</div>