<div class="container">

    <div class="clear row pad-top-5">

        <div class="col-12">
            <h1>Bem Vindo <?= $nome; ?></h1>
        </div>
    
    </div>
        
    <div class="clear row pad-top-5">
        
        <div class="col-3 menu-item">
            <a href="<?= base_url("usuario/todos"); ?>">Usuários</a>
        </div>
        
    </div>
        
    <div class="clear row pad-top-5">
        
        <div class="col-3 menu-item">
            <a href="<?= base_url("enquete/"); ?>">Enquetes</a>
        </div>

    </div>

</div>