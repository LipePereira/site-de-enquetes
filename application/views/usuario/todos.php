<div class="container">
    
    <div class="row clear pad-top-10">
        
        <h1>Usuários</h1>
        
    </div>
    
    <div class="row clear">
        
        <div class="col-4">
            <div class="col-12 menu-item margin-top">
                <a href="<?= base_url("usuario/todos"); ?>">Usuários</a>
            </div>
            
            <div class="col-12 menu-item margin-top">
                <a href="<?= base_url("usuario/cadastrar"); ?>">Cadastrar Novo</a>
            </div>
        </div>
        
        <div class="col-8">
        
            <table>
                <tr>
                    <th>
                        Nome
                    </th>
                    <th>
                        Email
                    </th>
                    <th>
                        Ações
                    </th>
                </tr>
                
                <?php foreach($usuarios as $usuario) { ?>
                <tr>
                    <td>
                        <?= $usuario->nome; ?>
                    </td>
                    <td>
                        <?= $usuario->email; ?>
                    </td>
                    <td>
                        <a href="<?= base_url("usuario/deletar/" . $usuario->id); ?>">Deletar</a>
                    </td>
                </tr>
                <?php } ?>
            </table>
            
        </div>
        
    </div>
    
</div>