<div class="container">
    
    <div class="clear row pad-top-5">
        <h1>Cadastrar Usuário</h1>
    </div>

<form action="<?= base_url("usuario/cadastrar"); ?>" method="post">
    
    <div class="field-group row clear pad-top-5">
        <label for="nome" class="col-3 text-right">Nome: </label>
        <input type="text" id="nome" name="nome" value="<?= $usuario->nome; ?>" class="col-9"/>
    </div>
    
    <div class="field-group row clear pad-top-5">
        <label for="email" class="col-3 text-right">Email: </label>
        <input type="text" name="email" value="<?= $usuario->email; ?>" class="col-9"/>
    </div>
    
    <div class="field-group row clear pad-top-5">
        <label for="email" class="col-3 text-right">Senha: </label>
        <input type="text" name="senha" value="" class="col-9"/>
    </div>
    
    <div class="field-group row clear pad-top-5">
        <label for="email" class="col-3 text-right">Confirme a Senha: </label>
        <input type="text" name="senha2" value="" class="col-9"/>
    </div>
    
    <div class="field-group row clear pad-top-5">
        <label for="email" class="col-3 text-right">Registro: </label>
        <input type="text" name="registro" value="<?= $usuario->registro; ?>" class="col-9"/>
    </div>
    
    <div class="field-group row clear pad-top-5">
        <label for="email" class="col-3 text-right">Tipo: </label>
        <select name="tipo" class="col-9">
            <option value="administrador">Administrador</option>
            <option value="funcionario">Funcionário</option>    
        </select>
    </div>
    
    <?php if (isset($erro)) {
        if ($erro = "dados_incompletos"){ ?>
                
                <div class="row clear pad-top-10 pad-bottom-10">
                    <div class="col-9 push-3 text-center error">
                        Preencha todos os campos!
                    </div>
                </div>
            
            <?php
        }  
    } ?>
    
    <div class="clear row pad-top-10">
        <input type="submit" value="Finalizar Cadastro de Usuário" class="right"/>
    </div>
</form>

</div>