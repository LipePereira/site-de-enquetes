<div class="container">

    <div class="row clear pad-top-10">
        <h1><?= $enquete->titulo; ?></h1>
        <p><?= $enquete->descricao; ?></p>
    </div>

    <div class="row clear pad-top-10">
        <h2>Perguntas: </h2>
    </div>
    
    <div class="row clear pad-top-10">
        <h2>Nova Pergunta: </h2>
    </div>
        
    <form method="post">
    
    <div class="field-group row clear pad-top-10">
        <label for="pergunta" class="col-3 text-right">Pergunta: </label>
        <input type="text" id="pergunta" name="pergunta" class="col-9"/>
    </div>
    
    <div class="field-group row clear pad-top-10">
        <label for="select" class="col-3 text-right">Tipo da pergunta</label>
        <select name="tipo" id="select" class="col-9">
            <option value="aberta">Aberta (Resposta Livre)</option>
            <option value="fechada">Fechada (Multipla Escolha)</option>
        </select>
    </div>
    
    <div class="row clear pad-top-10">
        <fieldset id="form" class="col-9 push-3">
            <legend>
                Adicionar Alternativas
            </legend>
            
        </fieldset>
    </div>
    
    <div class="row clear pad-top-10">
        <input type="submit" value="Salvar Pergunta" class="col-9 push-3"/>
    </div>

    <div class="row clear pad-top-10">
        <table>
            <tr>
                <th>Pergunta</th>
                <th>Tipo</th>
                <th>Ações</th>
            </tr>
        <?php 
            if ($perguntas) { 
                foreach($perguntas as $pergunta){
        ?>
                    <tr>
                        <td><?= $pergunta->pergunta ?></td>
                        <td><?= $pergunta->tipo ?></td>
                        <td><a href="<?= base_url("pergunta/remover/" . $pergunta->id); ?>">Remover</a></td>
                    </tr>
        <?php 
                }
            } 
        ?>
        </table>
    </div>
    
    </form>

</div>