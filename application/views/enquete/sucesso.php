<div class="container">

    <div class="row clear pad-top-10">
        <h1>Enquete Criada com Sucesso!</h1>
    </div>

    <div class="row clear pad-top-10">
        <h2><?= $enquete->titulo; ?></h2>
        <p><?= $enquete->descricao; ?></p>
    </div>

    <div class="row clear pad-top-10">
        <div class="col-4 menu-item">
            <a href="<?= base_url("pergunta/adicionar/" . $enquete->id); ?>">Adicionar Perguntas</a>
        </div>
    </div>

</div>