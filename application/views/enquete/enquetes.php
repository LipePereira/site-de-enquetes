<div class="container">

        <div class="row clear pad-top-10">
                <h1>Enquetes</h1>
                
                <div class="col-4 menu-item">
                    <a href="<?= base_url("enquete/nova"); ?>">Nova Enquete</a>
                </div>
        </div>
        
        <div class="row clear pad-top-10">
                <table>
                        <tr>
                                <th>Titulo</th>
                                <th>Descrição</th>
                                <th>Respondidas</th>
                                <th>Ações</th>
                        </tr>
                <?php foreach($enquetes as $key => $enquete) { ?>
                        
                        <tr>
                        
                                <th><?= $enquete->titulo; ?></th>
                                <td><?= $enquete->descricao; ?></td>
                                
                                <td>Respondidas: <?= $respondidas[$key]; ?> de <?= count($perguntas[$key]); ?></td>
                            
                                <td>
                                        <?php if ($respondidas[$key] != count($perguntas[$key])) { ?>
                                        <a href="<?= base_url("enquete/responder/" . $enquete->id); ?>">Responder</a>
                                        <?php } else { ?>
                                        Já Respondida!
                                        <?php } ?>
                                        <?php if ($tipo=="administrador") { ?> <a href="<?= base_url("enquete/editar/" . $enquete->id); ?>">Editar</a> <?php } ?>
                                        <?php if ($tipo=="administrador") { ?> <a href="<?= base_url("enquete/resultados/" . $enquete->id); ?>">Resultados</a> <?php } ?>
                                        <a href="<?= base_url("melhoria/enquete/" . $enquete->id); ?>">Melhorias</a>
                                </td>
                    
                        </tr>
                <?php } ?>
                </table>
        </div>
</div>