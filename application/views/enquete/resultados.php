<div class="container">
    
    <div class="clear row pad-top-10">
        
        <h1>Resultados da Enquete</h1>
        
        <h2><?= $enquete->titulo; ?></h2>
        
        <p><?= $enquete->descricao; ?></p>
        
    </div>
    
    <?php foreach ($perguntas as $pergunta) { ?>
        <div class="clear row pad-top-10">
            <h3>Pergunta: <?= $pergunta->pergunta; ?></h3>
            
            <table class="col-8">
                <tr>
                   <th>Respostas</th>
                   <th>Ocorrências</th>
                </tr>
            
            <?php if ($respostas[$pergunta->id]) { ?>
                <?php if ($alternativas[$pergunta->id]){ ?>
                
                    <?php foreach ($alternativas[$pergunta->id] as $alternativa) { ?>
                    
                    <tr>
                    
                        <?php if (isset($quantidades[$pergunta->id][$alternativa->resposta])){ ?> 
                            <td><?= $alternativa->resposta; ?></td>
                            <td><?= $quantidades[$pergunta->id][$alternativa->resposta]; ?></td>
                        <?php } else { ?>
                            <td><?= $alternativa->resposta; ?></td>
                            <td>0</td>
                        <?php } ?>
                        
                    </tr>
                        
                    <?php } ?>
                    
                <?php } else { ?>
                
                    <?php foreach ($quantidades[$pergunta->id] as $resposta => $quantidade) { ?>
                    <tr>
                
                        <td><?= $resposta; ?></td> 
                        <td><?= $quantidade; ?></td>
                        
                    </tr>
                    <?php } ?>

                <?php } ?>
            <?php } ?>
            
            </table>
            
            <div class="col-4">
                <canvas id="chart<?= $pergunta->id; ?>" width="400" height="400"></canvas>
                <script>
                var ctx = document.getElementById("chart<?= $pergunta->id; ?>");
                var myChart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: [
                            <?php foreach($quantidades[$pergunta->id] as $resposta => $quantidade) { ?>
                            "<?= $resposta; ?>",
                            <?php } ?>
                            ],
                        datasets: [{
                            label: 'Ocorrências',
                            data: [
                                <?php foreach($quantidades[$pergunta->id] as $resposta => $quantidade) { ?>
                                    <?= $quantidade; ?>,
                                <?php } ?>
                                ],
                            backgroundColor: [
                                <?php foreach($quantidades[$pergunta->id] as $resposta => $quantidade) { ?>
                                    randomColor({format: 'rgb'}),
                                <?php } ?>
                                ]
                        }]
                    },
                });
                </script>
            </div>
            
        </div>
    <?php } ?>
        
    </div>
    
</div>