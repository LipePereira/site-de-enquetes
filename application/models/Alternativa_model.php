<?php

class Alternativa_model extends CI_Model{
    public function __construct()
        {
                parent::__construct();
        }
        
    public $id;
    public $resposta;
    
    public $pergunta;
    
    public function create_on_db(){
        $this->db->insert("alternativas", $this);
    }
    
    public function retrieve_from_db_all(){
        $query = $this->db->get("alternativas");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $alternativas = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->resposta = $row->resposta;
            $this->pergunta = $row->pergunta;
            
            array_push($alternativas, clone $this);
        }
        
        return $alternativas;
    }
    
    public function retrieve_from_db_pergunta($pergunta){
        $this->db->where("pergunta", $pergunta);
        $query = $this->db->get("alternativas");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $alternativas = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->resposta = $row->resposta;
            $this->pergunta = $row->pergunta;
            
            array_push($alternativas, clone $this);
        }
        
        return $alternativas;
    }
    
    
    public function retrieve_from_db_id($id){
        $this->db->where("id", $id);
        $query = $this->db->get("alternativas");
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->resposta = $row->resposta;
            $this->pergunta = $row->pergunta;
            
            return true;
        }
        
        return false;
    }
}

?>