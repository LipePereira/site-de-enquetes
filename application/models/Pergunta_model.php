<?php

class Pergunta_model extends CI_Model{
    public function __construct()
        {
                parent::__construct();
        }
        
    public $id;
    public $pergunta;
    public $tipo;
    
    public $enquete;
    
    public function create_on_db(){
        $this->db->insert("perguntas", $this);
        return $this->db->insert_id();
    }
    
    public function delete_from_db(){
        $this->db->where("id", $this->id);
        $this->db->delete("perguntas");
    }
    
    public function retrieve_from_db_all(){
        $query = $this->db->get("perguntas");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $perguntas = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->pergunta = $row->pergunta;
            $this->tipo = $row->tipo;
            $this->enquete = $row->enquete;
            
            array_push($perguntas, clone $this);
        }
        
        return $perguntas;
    }
    
    public function retrieve_from_db_enquete($enquete){
        $this->db->where("enquete", $enquete);
        $query = $this->db->get("perguntas");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $perguntas = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->pergunta = $row->pergunta;
            $this->tipo = $row->tipo;
            $this->enquete = $row->enquete;
            
            array_push($perguntas, clone $this);
        }
        
        return $perguntas;
    }
    
    public function retrieve_from_db_id($id){
        $this->db->where("id", $id);
        $query = $this->db->get("perguntas");
        
        $perguntas = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->pergunta = $row->pergunta;
            $this->tipo = $row->tipo;
            $this->enquete = $row->enquete;
            
            return $this;
        }
        
        return false;
    }
    
}

?>