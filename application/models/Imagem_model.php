<?php

class Imagem_model extends CI_Model{
    public function __construct()
        {
                parent::__construct();
        }
        
    public $id;
    public $url;
    public $descricao;
    
    public $melhoria;
    
    public function create_on_db(){
        $this->db->insert("imagens", $this);
    }
    
    public function delete_from_db(){
        $this->db->where("id", $this->id);
        $this->db->delete("imagens");
    }
    
    public function retrieve_from_db_id($id){
        $this->db->where("id", $id);
        $query = $this->db->get("imagens");
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->url = $row->url;
            $this->descricao = $row->descricao;
            $this->melhoria = $row->melhoria;
            
            return true;
        }
        
        return false;
    }
    
    public function retrieve_from_db_all(){
        $query = $this->db->get("imagens");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $imagens = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->url = $row->url;
            $this->descricao = $row->descricao;
            $this->melhoria = $row->melhoria;
            
            array_push($imagens, clone $this);
        }
        
        return $imagens;
    }
    
    public function retrieve_from_db_melhoria($melhoria){
        $this->db->where("melhoria", $melhoria);
        $query = $this->db->get("imagens");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $imagens = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->url = $row->url;
            $this->descricao = $row->descricao;
            $this->melhoria = $row->melhoria;
            
            array_push($imagens, clone $this);
        }
        
        return $imagens;
    }
}

?>