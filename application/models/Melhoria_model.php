<?php

class Melhoria_model extends CI_Model{
    public function __construct()
        {
                parent::__construct();
        }
        
    public $id;
    public $titulo;
    public $descricao;
    
    public $enquete;
    
    public function create_on_db(){
        $this->db->insert("melhorias", $this);
        return $this->db->insert_id();
    }
    
    public function delete_from_db(){
        $this->db->where("id", $this->id);
        $this->db->delete("melhorias");
    }
    
    public function retrieve_from_db_id($id){
        $this->db->where("id", $id);
        $query = $this->db->get("melhorias");
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->titulo = $row->titulo;
            $this->descricao = $row->descricao;
            $this->enquete = $row->enquete;
            
            return true;
        }
        
        return false;
    }
    
    public function retrieve_from_db_all(){
        $query = $this->db->get("melhorias");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $melhorias = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->titulo = $row->titulo;
            $this->descricao = $row->descricao;
            $this->enquete = $row->enquete;
            
            array_push($melhorias, clone $this);
        }
        
        return $melhorias;
    }
    
    public function retrieve_from_db_enquete($enquete){
        $this->db->where("enquete", $enquete);
        $query = $this->db->get("melhorias");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $melhorias = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->titulo = $row->titulo;
            $this->descricao = $row->descricao;
            $this->enquete = $row->enquete;
            
            array_push($melhorias, clone $this);
        }
        
        return $melhorias;
    }
}

?>