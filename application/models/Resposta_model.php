<?php

class Resposta_model extends CI_Model{
    public function __construct()
        {
                parent::__construct();
        }
        
    public $id;
    public $valor;
    
    public $pergunta;
    public $alternativa;
    public $usuario;
    
    public function create_on_db(){
        $this->db->insert("respostas", $this);
    }
    
    public function delete_from_db(){
        $this->db->where("id", $this->id);
        $this->db->delete("respostas");
    }
    
    public function retrieve_from_db_all(){
        $query = $this->db->get("respostas");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $respostas = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->valor = $row->valor;
            $this->pergunta = $row->pergunta;
            $this->alternativa = $row->alternativa;
            $this->usuario = $row->usuario;
            
            array_push($respostas, clone $this);
        }
        
        return $respostas;
    }
    
    public function retrieve_from_db_pergunta($pergunta){
        $this->db->where("pergunta", $pergunta);
        $query = $this->db->get("respostas");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $respostas = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->valor = $row->valor;
            $this->pergunta = $row->pergunta;
            $this->alternativa = $row->alternativa;
            $this->usuario = $row->usuario;
            
            array_push($respostas, clone $this);
        }
        
        return $respostas;
    }
    
    public function retrieve_from_db_usuario_pergunta($usuario, $pergunta){
        $this->db->where(array(
            "usuario" => $usuario,
            "pergunta" => $pergunta
            ));
        $query = $this->db->get("respostas");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $respostas = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->valor = $row->valor;
            $this->pergunta = $row->pergunta;
            $this->alternativa = $row->alternativa;
            $this->usuario = $row->usuario;
            
            array_push($respostas, clone $this);
        }
        
        return $respostas;
    }
    
    public function retrieve_from_db_id($id){
        $this->db->where("id", $id);
        $query = $this->db->get("respostas");
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->valor = $row->valor;
            $this->pergunta = $row->pergunta;
            $this->alternativa = $row->alternativa;
            $this->usuario = $row->usuario;
            
            return true;
        }
        
        return false;
    }
}

?>