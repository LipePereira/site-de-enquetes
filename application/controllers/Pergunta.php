<?php
    class Pergunta extends CI_Controller{
        function adicionar($enquete){
            if ($this->session->userdata("tipo") != "administrador") {
                redirect(base_url("usuario/login/apenas_administrador"));
            }
            
            $this->load->view("base/header");
            
            $this->load->model("Enquete_model", "enquete");
            $this->load->model("Pergunta_model", "pergunta");
            $this->load->model("Alternativa_model", "alternativa");
            
            if ($this->enquete->retrieve_from_db_id($enquete)){
                
                if ($this->input->post() != null) {
                    $this->pergunta->id = null;
                    $this->pergunta->pergunta = $this->input->post("pergunta");
                    $this->pergunta->tipo = $this->input->post("tipo");
                    $this->pergunta->enquete = $enquete;
                    
                    $this->pergunta->id = $this->pergunta->create_on_db();
                    
                    if ($this->pergunta->tipo == "fechada"){
                        foreach ($this->input->post("alternativas") as $alternativa){
                            $this->alternativa->id = null;
                            $this->alternativa->resposta = $alternativa;
                            $this->alternativa->pergunta = $this->pergunta->id;
                            
                            $this->alternativa->create_on_db();
                        }
                    }
                }
                
                $data["enquete"] = $this->enquete;
                $data["perguntas"] = $this->pergunta->retrieve_from_db_enquete($enquete);
                $this->load->view("pergunta/adicionar", $data);
                
            } else {
                echo "Enquete não existe, tente outra";
            }
            
            $this->load->view("base/footer");
        }
        
        function remover($pergunta){
            if ($this->session->userdata("tipo") != "administrador") {
                redirect(base_url("usuario/login/apenas_administrador"));
            }
            
            $this->load->model("Pergunta_model", "pergunta");
            $this->pergunta->retrieve_from_db_id($pergunta);
            
            $enquete = $this->pergunta->enquete;
            
            $this->pergunta->delete_from_db();
            
            redirect(base_url("enquete/editar/" . $enquete . "/pergunta_excluida"));
        }
    }
?>