<?php
    class Imagem extends CI_Controller{
        function adicionar($melhoria){
            if ($this->session->userdata("tipo") != "administrador") {
                redirect(base_url("usuario/login/apenas_administrador"));
            }
        
            $this->load->model("Imagem_model", "imagem");
            
            if ($this->input->post() != null){
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = '*';
                
                $this->load->library('upload', $config);
                
                if ( ! $this->upload->do_upload("imagem")) {
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                } else {
                    $this->imagem->id = null;
                    $this->imagem->url = base_url("uploads/" . $this->upload->data("file_name"));
                    $this->imagem->descricao = $this->input->post("descricao");
                    $this->imagem->melhoria = $melhoria;
                    
                    $this->imagem->create_on_db();
                    
                }
            }
            
            $this->load->view("base/header");
            
            $this->load->model("Melhoria_model", "melhoria");
            $this->melhoria->retrieve_from_db_id($melhoria);
            
            $data["melhoria"] = $this->melhoria;
            $data["imagens"] = $this->imagem->retrieve_from_db_melhoria($melhoria);
            
            $this->load->view("imagem/adicionar", $data);
            
            $this->load->view("base/footer");
        }
        
        function remover($id){
            $this->load->model("Imagem_model", "imagem");
            $this->imagem->retrieve_from_db_id($id);
            
            $broken = explode("/", $this->imagem->url);
            $link = $broken[count($broken) - 1];
            
            $remove = "./uploads/" . $link;
            echo $remove;
            unlink($remove);
            
            $melhoria = $this->imagem->melhoria;
            
            $this->imagem->delete_from_db();
            
            redirect(base_url("melhoria/detalhes/" . $melhoria));
        }
    }
?>