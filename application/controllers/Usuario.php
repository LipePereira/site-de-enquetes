<?php

class Usuario extends CI_Controller{
    
    function index(){
        $this->load->view("base/header");
        
        if ($this->session->userdata("logged")){
            $data = array();
            $data["nome"] = $this->session->userdata("nome");
            $this->load->view("usuario/home", $data);
        } else {
            redirect(base_url("usuario/login/"));
        }
        
        $this->load->view("base/footer");
    }
    
    function login($param = ""){
        $this->load->view("base/header");
        
        $dados = array();
        
        $dados["erro"] = $param;
        
        $this->load->view("usuario/login", $dados);
        
        $this->load->view("base/footer");
    }
    
    function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }
    
    function entrar(){
        $username = $this->input->post("username");
        $password = $this->input->post("password");
        
        $this->load->model("Usuario_model", "usuario");
        if ($this->usuario->retrieve_from_db_credentials($username, $password)){
            $userdata = array(
                "nome" => $this->usuario->nome,
                "id" => $this->usuario->id,
                "tipo" => $this->usuario->tipo,
                "logged" => true
                );
            $this->session->set_userdata($userdata);
            redirect(base_url('usuario/'));
        } else {
            redirect(base_url('usuario/login/credenciais_invalidas'));
        }
    }
    
    function cadastrar(){
        if ($this->session->userdata("tipo") != "administrador") {
            redirect(base_url("usuario/login/apenas_administrador"));
        }
        
        $this->load->view("base/header");
        
        $this->load->model("Usuario_model", "usuario");
        
        if ($this->input->post() == null) {
            $data = array();
            $data["usuario"] = $this->usuario;
            $this->load->view("usuario/cadastro", $data);
        } else {
            $this->usuario->nome = $this->input->post("nome");
            $this->usuario->email = $this->input->post("email");
            if ($this->input->post("senha") == $this->input->post("senha2") && $this->input->post("senha") != ""){
                $this->usuario->senha = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
            } else {
                $this->usuario->senha = "";
            }
            $this->usuario->registro = $this->input->post("registro");
            $this->usuario->tipo = $this->input->post("tipo");
            
            if ($this->usuario->check_all_filled()){
                $this->usuario->create_on_db();
                redirect(base_url("usuario/"));
            } else {
                $data = array();
                $data["usuario"] = $this->usuario;
                $data["erro"] = "dados_incompletos";
                $this->load->view("usuario/cadastro", $data);
            }
        }
        
        $this->load->view("base/footer");
        
    }
    
    function todos(){
        $this->load->view("base/header");
        
        $this->load->model("Usuario_model", "usuario");
        $data["usuarios"] = $this->usuario->retrieve_from_db_all();
        
        $this->load->view("usuario/todos", $data);
        
        $this->load->view("base/footer");
    }
    
    function deletar($id){
        $this->load->model("Usuario_model", "usuario");
        $this->usuario->retrieve_from_db_id($id);
        $this->usuario->delete_from_db();
        
        redirect(base_url("usuario/todos"));
    }

    // function createuser(){
    //     $this->load->model("Usuario_model", "usuario");
        
    //     $this->usuario->id = "";
    //     $this->usuario->nome = "Felipe";
    //     $this->usuario->senha = password_hash("qwe123", PASSWORD_DEFAULT);
    //     $this->usuario->email = "felipe@pereira.com";
    //     $this->usuario->registro = "12345";
    //     $this->usuario->tipo = "administrador";
        
    //     $this->usuario->create_on_db();
        
    // }
}

?>